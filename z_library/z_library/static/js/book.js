
$(function () {
           
            // 表头标签
            var sheetTitle = '<tr class="sheetTitle"><th>图书编号</th><th>图书名字</th><th>图书作者</th><th>书中人物</th><th>出版时间</th><th>阅读数</th><th>评论数</th></tr>'
            
            // 1. 数据标签
            function fnDataElementTr(data) {

                var dataTr = '<tr>' 
                                + '<td class="idInput"><input type="text" value=' + data.id +'></td>'
                                + '<td><input type="text" value=' +data.name+'></td>'
                                + '<td><input type="text" value=' +data.author+'></td>'
                                + '<td><input type="text" value=' +data.hero+'></td>'
                                + '<td><input type="text" value=' +data.time+'></td>'
                                + '<td><input type="text" value=' +data.read+'></td>'
                                + '<td><input type="text" value=' +data.comment+'></td>'
                                + '<td class="del"><input  type="button" value=" 删 除 "></td>'
                                + '<td class="update"><input type="button" value=" 修 改 "></td>'
                                + '</tr>'
                    
                return dataTr
                
            }
            
            // 1.首页初始化加载数据
            function fnLoadHomeData(judage) {
                $.get({
                    url:"/books/",
                    dataType:'json',
                    success:function (data) {
                        content = sheetTitle
                        for (let index = 0; index < data.length; index++) {
                            var dat = data[index];
                             //  1. 拼接数据
                           content += fnDataElementTr(dat)  
                        }
                       // 2. 插入数据
                       $('.booklist').html(content)
                        
                       if (judage == "del") {
                        $('.del').show()
                       } else if (judage == "update") {
                        $('.update').show()  
                       }
                       $('.idInput').on('focus',function () {
                           alert('2332')
                        $(this).blur()
                    }) 
                       }

                   });
            }
            fnLoadHomeData()
            
            // 1.左侧操作按钮的 交互点击
            var $leftBtns = $('button');
            $leftBtns.click(function () {
                $(this).addClass('leftbtn').parent().siblings().children().removeClass('leftbtn');
            });
                    

            //  查询图书按钮 获取所有的图书信息展示
            $('.checkBook').click(function () {
               
                $('.booklist').show()
                $('.addlist').hide()
                $('.del').hide()
                $('.update').hide()
               
            })  
            
            //  增加图书按钮 
            $('.addBook').click(function () {
                $('.booklist').hide()
                $('.addlist').show()
                // 监听增加按钮
                $('.add').on('click',function () {
                    var addTds = $('.addlist input')
                    dict_data = {}
                    for (var i=0;i < (addTds.length-1); i++){
                        if (i==0) {
                            dict_data.id = addTds.eq(i).val()
                        } else if(i==1){
                            dict_data.name = addTds.eq(i).val()
                        }else if(i==2){
                            dict_data.author = addTds.eq(i).val()
                        }else if(i==3){
                            dict_data.hero = addTds.eq(i).val()
                        }else if(i==4){
                            dict_data.time = addTds.eq(i).val()
                        }else if(i==5){
                            dict_data.read = addTds.eq(i).val()
                        }else if(i==6){
                            dict_data.comment = addTds.eq(i).val()
                        }
                    }
                    if (dict_data.name == ""|dict_data.author == ""|dict_data.hero == ""|dict_data.time == ""|dict_data.read == ""|dict_data.comment == ""){
                        alert('输入内容不能为空!')
                        return
                    }
                    $.post({
                        url:"/books/",
                        dataType:"json",
                        data:dict_data,
                        success:function (dat) {
                            alert(dat.data)
                        }
                    })
                    // 清空所有 输入框!
                    for (var i=0;i < (addTds.length-1); i++){
                        console.log(i)
                        addTds.eq(i).val("")
                    }
                    fnLoadHomeData('add')
                })

            })
            
            //  删除图书 删除图书按钮 设置 一个
            $('.delBook').click(function () {
                $('.booklist').show()
                $('.addlist').hide()
                $('.del').show()
                $('.update').hide()
                    // 监听删除按钮
                    $('.del').on('click',function () {
                        result = $(this).siblings().eq(0).children('input').val()
                        $.ajax({
                            url:'/books/',
                            dataType:'json',
                            type:'delete',
                            data:JSON.stringify({id:result}),
                            success:function(dat){
                                alert(dat.data)
                                
                                fnLoadHomeData('del')
                            }
                        })
                
                });

                
            })
            
            //  修改图书
            $('.updateBook').click(function () {
                $('.booklist').show()
                $('.addlist').hide()
                $('.del').hide()
                $('.update').show()
                // 监听修改按钮
                $('.update').on('click',function () {
                        var tds = $(this).siblings().children()
                      
                        dict_data = {
                            "id":tds.eq(0).val(),
                            "name":tds.eq(1).val(),
                            "author":tds.eq(2).val(),
                            "hero":tds.eq(3).val(),
                            "time":tds.eq(4).val(),
                            "read":parseInt(tds.eq(5).val()),
                            "comment":tds.eq(6).val()
                        }
                        $.ajax({
                            url:"/books/",
                            type:"put",
                            dataType:'json',
                            data:JSON.stringify(dict_data),
                            success:function (dat){
                                alert(dat.data)
                                fnLoadHomeData('update')
                            }
                        })
                 })
                
            })
    })   
    