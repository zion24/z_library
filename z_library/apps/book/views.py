from django.shortcuts import render

# Create your views here.
from django.views.generic import View
from django.http.response import JsonResponse


# Create your views here.


class BookListView(View):
    """
    图书列表
    """

    def get(self, request):
        return JsonResponse({'msg': 'ok'})
